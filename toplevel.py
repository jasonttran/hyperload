"""
Hyperload - Top level
"""

import argparse
import sys

from hyperload import Hyperload

__author__ = "jtran"
__version__ = "1.0.0"


def get_args():
    arg_parser = argparse.ArgumentParser()
    serial_arguments = arg_parser.add_argument_group("Serial Arguments")
    arg_parser.add_argument("-i", "--input",
                            type=str,
                            metavar="<file path>",
                            required=True,
                            help="Firmware HEX file path")
    serial_arguments.add_argument("-p", "--port",
                                  type=str,
                                  metavar="<port identifier>",
                                  required=True,
                                  help="Serial port (i.e. COM6 or /dev/tty.cuserial or /dev/ttyUSB0")
    return arg_parser.parse_args()


def main():
    args = get_args()
    hex_filepath = args.input
    port = args.port

    hyperload = Hyperload(serial_port=port)
    error = hyperload.update(hex_filepath)
    return error


if __name__ == "__main__":
    sys.exit(main())
