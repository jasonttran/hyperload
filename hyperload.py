"""
Hyperload
"""

import time

from serial import Serial

from base import HyperloadABC, DEFAULT_SERIAL_BAUD

__author__ = "jtran"
__version__ = "1.1.0"


class Hyperload(HyperloadABC):
    def __init__(self, serial_port, serial_baud=DEFAULT_SERIAL_BAUD, **kwargs):
        super(Hyperload, self).__init__(**kwargs)
        self._serial_port = serial_port
        self._serial_baud = serial_baud
        self._serial = Serial(port=serial_port, baudrate=serial_baud)

    """
    Public methods
    """
    def close(self):
        self._serial.close()

    """
    Abstract methods
    """
    def _serial_write(self, data):
        length_written = self._serial.write(data)
        return int(not length_written == len(data))

    def _serial_read(self, length=1):
        message = ""
        if self._serial.is_open:
            message = self._serial.read(size=length)
        return message

    def _serial_change_baud(self, new_baud):
        self._serial.baudrate = new_baud

    def _reset(self):
        if self._serial.is_open:
            self._serial.rts = True
            self._serial.dtr = True
            time.sleep(0.1)
            self._serial.reset_input_buffer()
            self._serial.reset_output_buffer()
            self._serial.flush()
            self._serial.rts = False
            self._serial.dtr = False




if __name__ == "__main__":  # Test
    _filepath = "lpc1758_freertos.hex"
    _hyperload = Hyperload(serial_port="COM6")
    print _hyperload.update(_filepath)
