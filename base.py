"""
Hyperload abstract base class for SJOne

References:
    - http://socialledge.com/sjsu/index.php/Hyperload_Protocol
    - https://github.com/kammce/Hyperload
"""

from abc import ABCMeta, abstractmethod
import logging
import re
import sys

from intelhex import IntelHex

from board_description import BoardDescription

__author__ = "jtran"
__version__ = "2.0.0"

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

AVAILABLE_SERIAL_BAUD_FAST_MODE = [
    57600,
    72000,
    100000,
    115200,
    125000,
    144000,
    230400,
    250000,
    500000,
    1000000,
]

DEFAULT_CPU_SPEED = 48*1000*1000  # 48 MHz
DEFAULT_SERIAL_BAUD = 38400
DEFAULT_SERIAL_BAUD_FAST_MODE = AVAILABLE_SERIAL_BAUD_FAST_MODE[-1]
BOARD_DESCRIPTION_PATTERN = "^\$([a-zA-Z0-9]+):([0-9]+):([0-9]+):([0-9]+)$"  # Example: "$LPC1758:4096:2048:512"


class HyperloadABC(object):
    # Hyperload bootloader special codes
    DOLLAR = "$"
    ACK = "!"
    EXITING_TO_APP = "*"
    NEWLINE = "\n"

    CHECKSUM_ERROR = "@"
    VERIFY_ERROR = "^"

    __metaclass__ = ABCMeta

    def __init__(self, cpu_speed=DEFAULT_CPU_SPEED, serial_baud_fast_mode=DEFAULT_SERIAL_BAUD_FAST_MODE, logger=None):
        self._cpu_speed = cpu_speed
        self._serial_baud_fast_mode = serial_baud_fast_mode
        self._board_description = None

        self._logger = logger if (logger is not None) else logging.getLogger()

    """
    Public methods
    """
    def update(self, *args, **kwargs):
        """
        Perform Hyperload firmware update
        :return: Error code (int)
        """
        error = 0
        for percent, error in self.update_iter(*args, **kwargs):
            self._logger.info("Progress: %s%", percent)
            if error:
                break
        if error:
            self._logger.error("Firmware update failed! Error: %s", error)
        return error

    def update_iter(self, program_filepath):
        """
        Perform Hyperload firmware update generator
        :param program_filepath: Firmware file path (str)
        :yield: Progress percentage (int), error code (int)
        """
        percent = 0
        intel_hex_obj = IntelHex(program_filepath)

        self._logger.debug("Performing reset...")
        self._reset()

        self._logger.debug("Performing handshake...")
        error = self._perform_handshake()
        if error:
            self._logger.error("Handshake failed! Error: %s ", error)
            yield percent, error  # Return early

        self._logger.debug("Switching to serial fast mode...")
        error = self._switch_to_serial_fast_mode()
        if error:
            self._logger.error("Failed to switch to serial fast mode! Error: %s", error)
            yield percent, error  # Return early

        self._logger.debug("Waiting for board description...")
        error = self._handle_board_description()
        if error:
            self._logger.error("Failed to get board description! Error: %s", error)
            yield percent, error  # Return early
        else:
            self._logger.debug("\n%s", self._board_description)

        error = self._expect_acknowledge()
        if error:
            self._logger.error("Failed to get ACK! Error: %s", error)
            yield percent, error  # Return early

        self._logger.debug("Transferring program...")
        for percent in self._transfer_binary_content_iter(intel_hex_obj):
            yield percent, error

        if percent < 100:
            self._logger.error("Failed to transfer program!")
            yield percent, 1  # Return early

        self._logger.debug("Finalizing transaction...")
        error = self._finalize_transaction()
        if error:
            self._logger.error("Failed to finalize transaction! Error: %s", error)
            yield percent, error  # Return early

    """
    Private methods
    """
    def _expect_acknowledge(self):
        """
        Expect acknowledgement ACK
        :return: Error code (int)
        """
        data = self._serial_read(1)
        data = data if isinstance(data, str) else chr(data)
        return int(not (data == self.ACK))

    def _perform_handshake(self):
        """
        Perform SJOne handshake
        :return: Error code (int)
        """
        error = 0
        for index, data in enumerate(['\xff', '\x55', '\xAA']):
            if index == 0:
                rx = self._serial_read(1)
                if rx != data:
                    error = 1
                    break
            elif index == 1:
                err = self._serial_write(data)
                if err:
                    error = 2
                    break
            else:  # index == 2
                rx = self._serial_read(1)
                if rx != data:
                    error = 3
                    break
        return error

    def _switch_to_serial_fast_mode(self):
        """
        Switch to serial fast mode
        :return: Error code (int)
        """
        control_word = self._calculate_control_word(baud=self._serial_baud_fast_mode, cpu_speed=DEFAULT_CPU_SPEED)
        error = self._serial_write(data=chr(control_word))
        if error:
            return 1  # Return early

        response = self._serial_read(1)
        if ord(response) != control_word:
            return 2  # Return early

        self._serial_change_baud(new_baud=self._serial_baud_fast_mode)
        return error

    def _handle_board_description(self):
        """
        Handle board description
        Expect message from target device: "$<Device>:<Block size>:<Boot size[words]>:<Flash size[KB}>\n"
        Example: "$LPC1758:4096:2048:512\n"
        :return: Error code (int)
        """
        board_description_str = ""
        start_code = self._serial_read(1)
        if start_code != self.DOLLAR:
            return 1  # Return early
        else:
            board_description_str += start_code

        while True:
            data = self._serial_read(1)
            if data != self.NEWLINE:
                board_description_str += data
            else:
                break

        board_description = self._decode_board_description(board_description_str)
        self._board_description = board_description
        return 0

    def _transfer_binary_content_iter(self, intel_hex_obj):
        """
        Binary transfer generator
        :param intel_hex_obj: Intel Hex obj (IntelHex)
        :yield: Progress percentage; 100 if completed successfully (int)
        """
        assert isinstance(intel_hex_obj, IntelHex)
        binary_array = intel_hex_obj.tobinarray()
        binary_array = bytearray(binary_array)

        block_count = len(binary_array) / self._board_description.block_size
        padding_count = len(binary_array) % self._board_description.block_size
        padding_count = padding_count if (block_count * self._board_description.block_size) > len(binary_array) else (self._board_description.block_size - padding_count)
        binary_array += '\x00' * padding_count
        block_count = len(binary_array) / self._board_description.block_size

        yield 0  # 0 percent

        err = 0
        for block_index in range(block_count):
            offset = block_index * self._board_description.block_size
            limit = min((offset + self._board_description.block_size), (block_count * self._board_description.block_size))
            block_content = binary_array[offset:limit]

            checksum = self._calculate_checksum(block_content)
            err |= self._serial_write(chr((block_index >> 8) & 0xFF))  # Send high address
            err |= self._serial_write(chr(block_index & 0xFF))  # Send low address
            err |= self._serial_write(block_content)  # Send block content
            err |= self._serial_write(chr(checksum))
            if err:
                self._logger.error("Failed to write during transfer!")
                break

            response = self._serial_read(1)
            if response == self.ACK:
                pass  # Do nothing
            elif response == self.CHECKSUM_ERROR:
                self._logger.error("Checksum error!")
                break
            elif response == self.VERIFY_ERROR:
                self._logger.error("Verify error!")
                break
            else:
                self._logger.error("Unexpected response during transfer! Response: %s", response)
                break

            yield int((float(block_index+1)/block_count) * 100)

    def _finalize_transaction(self):
        """
        Finalize binary transfer by sending a terminating address 0xFFFF
        :return: Error code (int)
        """
        end_transaction_message = [0xFF, 0xFF]
        error = self._serial_write(end_transaction_message)
        if error:
            self._logger.error("Failed to send transaction termination message!")
            return error  # Return early
        ack = self._serial_read()
        error = int(not ack == self.EXITING_TO_APP)
        return error

    """
    Private methods - Helper
    """
    @staticmethod
    def _decode_board_description(board_description_str):
        """
        Decode board description string
        Example: "$LPC1758:4096:2048:512"
        :param board_description_str: Board description string (str)
        :return: Board description object (BoardDescription)
        """
        match = re.match(pattern=BOARD_DESCRIPTION_PATTERN, string=board_description_str)
        new_board_description = BoardDescription(
            device=match.group(1),
            block_size=match.group(2),
            boot_size=match.group(3),
            flash_size=match.group(4)
        )
        return new_board_description

    @staticmethod
    def _calculate_control_word(baud, cpu_speed):
        """
        Calculate control word
        :param baud: Target serial baud
        :param cpu_speed: Target CPU speed
        :return: Control word (int)
        """
        control_word = ((cpu_speed / (baud * 16)) - 1)
        return control_word

    @staticmethod
    def _calculate_checksum(bytes):
        """
        Calculate checksum
        :param bytes: A block of bytes (list of int)
        :return: Checksum (int)
        """
        checksum = 0
        for byte in bytes:
            checksum = (checksum + byte) & 0xFF
        return checksum

    """
    Abstract methods
    """
    @abstractmethod
    def _serial_write(self, data):
        """
        Serial write
        :param data: Packet to transmit (chr) or packets to transmit (list of int) or (str)
        :return: Error code (int)
        """
        raise NotImplementedError

    @abstractmethod
    def _serial_read(self, length=1):
        """
        Serial read
        :param length: Number of packets to read (int)
        :return: String (str)
        """
        raise NotImplementedError

    @abstractmethod
    def _serial_change_baud(self, new_baud):
        """
        Change serial baud
        :param new_baud: Target baud (int)
        """
        raise NotImplementedError

    @abstractmethod
    def _reset(self):
        """
        Perform SJOne system reset
        """
        raise NotImplementedError




if __name__ == "__main__":  # Test

    class MockHyperLoad(HyperloadABC):
        def __init__(self):
            super(MockHyperLoad, self).__init__()
            self._board_description = BoardDescription("LPC1758", "4096", "2048", "512")

        def _serial_write(self, data):
            return 0

        def _serial_read(self, length=1):
            return "!"

        def _serial_change_baud(self, new_baud):
            return 0

        def _reset(self):
            return 0


    _filepath = "lpc1758_freertos.hex"
    _mock_hyperload = MockHyperLoad()
    _mock_hyperload.update(_filepath)
