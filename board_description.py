"""
Board description
"""

__author__ = "jtran"
__version__ = "1.0.0"


class BoardDescription(object):
    def __init__(self, device, block_size, boot_size, flash_size):
        self.device = str(device)
        self.block_size = int(block_size)  # Block size in B
        self.boot_size = int(boot_size)  # Boot size in words
        self.flash_size = int(flash_size)  # Flash size in KB

    def __str__(self):
        fmt = "Device: {}\n" \
              "Block size: {}\n" \
              "Boot size: {}\n" \
              "Flash size: {}\n"
        return fmt.format(self.device, self.block_size, self.boot_size, self.flash_size)




if __name__ == "__main__":  # Test
    _board_description = BoardDescription("LPC1758", "4096", "2048", "512")
    print _board_description.device, _board_description.block_size
    print _board_description
